;;========================================================================
;; factorial/test.sld 
;; a test library for a factorial library
;;========================================================================

(define-library (factorial-r7 test)
                (export test)
                (import (scheme base) (scheme write) (scheme read)
                        (factorial-r7 main))
                (begin
                  (define a-test (lambda (k l-factorial)
                                   (display k)
                                   (display "! = ")
                                   (display (l-factorial k))
                                   (newline)))

                  (define test (lambda (l-factorial)
                                 (a-test 1 l-factorial)
                                 (a-test 2 l-factorial)
                                 (a-test 4 l-factorial)
                                 (a-test 20 l-factorial)))))
