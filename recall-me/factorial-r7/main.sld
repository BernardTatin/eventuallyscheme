;;========================================================================
;; factorial/main.sld 
;; a facorial library
;;========================================================================

(define-library (factorial-r7 main)
                (export factorial-d factorial)
                (import (scheme base) (scheme write) (scheme read))

                (begin

                  (define factorial
                    (lambda(N)
                      (letrec ((ifactorial (lambda(acc k)
                                             (if (> k 1)
                                                 (let ((n-acc (* k acc))
                                                       (n-k (- k 1)))
                                                   (ifactorial n-acc n-k))
                                                 acc
                                                 ))))
                        (ifactorial 1 N))))

                  (define factorial-d
                    (lambda(N)
                      (define ifactorial (lambda(acc k)
                                           (if (> k 1)
                                               (let ((n-acc (* k acc))
                                                     (n-k (- k 1)))
                                                 (ifactorial n-acc n-k))
                                               acc
                                               )))
                      (ifactorial 1 N)))))

