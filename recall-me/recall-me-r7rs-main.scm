;;========================================================================
;; recall-me-r7rs-main.scm
;; a true r7rs sample program with libraries
;; in different SLD files 
;;========================================================================

(import (scheme base) (scheme write) (scheme read)
        (factorial-r7 main) (factorial-r7 test))
(test factorial)
(test factorial-d)

