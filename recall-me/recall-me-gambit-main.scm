;;========================================================================
;; recall-me-r6rs-main.scm
;; a true r6rs sample program with libraries
;; in different SLD files 
;;========================================================================

(load "factorial-r5/main-gambit.scm")
(load "factorial-r5/test.scm")

(test#test main#factorial)
(test#test main#factorial-d)
(exit)
