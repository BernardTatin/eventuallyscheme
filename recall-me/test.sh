#! /bin/bash
#
# test.sh
# Copyright (C) 2018 bernard <bernard@bernard-LIFEBOOK-E782>
#
# Distributed under terms of the MIT license.
#

gcode=recall-me-r7rs.scm 

dotest () {
   local scheme_compiler="$1"
   local lcode=${gcode}


   [[ $# -ge 2 ]] && lcode=$2
   local logfile="${scheme_compiler}-${lcode}.log"

   echo "${scheme_compiler} ${lcode}"
   eval "${scheme_compiler} ${lcode}" > "${logfile}" 
   diff -q "${logfile}" reference.txt \
      || echo "   ${scheme_compiler} failure"
}

for r5compiler in ikarus "csi -qb" gambit-ci "chezscheme9.5 -q" "petite9.5 -q" "csi -q"
do 
   dotest "${r5compiler}" recall-me-r5rs.scm 
done 

for r6compiler in ikarus "chezscheme9.5 -q" "petite9.5 -q"
do 
   dotest "${r6compiler}" recall-me-r6rs-main.scm 
done 

for r7compiler in "csi -qb -R r7rs" larceny foment gosh
do 
   dotest "${r7compiler}"
done 

for r7compiler in foment "gosh -I." "csi -qb -I. -R r7rs" larceny
do 
   dotest "${r7compiler}" recall-me-r7rs-main.scm
done 

for script in recall-me-r7rs.rkt recall-me-plait.rkt recall-me-racket.rkt
do 
   dotest mzscheme ${script}
done

# dotest larceny
# dotest foment 
# dotest ikarus recall-me-r5rs.scm 
# dotest gsi-script recall-me-r5rs.scm 
# dotest gosh
# dotest mzscheme recall-me-r7rs.rkt
# dotest mzscheme recall-me-plait.rkt
# dotest mzscheme recall-me-racket.rkt
