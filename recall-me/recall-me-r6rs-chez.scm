#!/usr/bin/chezscheme9.5 --program
;;========================================================================
;; recall-me-r6rs-chez.scm
;; a true r6rs for Chez Scheme only sample program with libraries
;; in different SLD files 
;;========================================================================

(import (rnrs) (factorial-r6 main) (factorial-r6 test))

;; startup procedure
;; doesn't work
;;
;; :(scheme-start
;; :  (lambda fns
;; :    (for-each
;; :      (lambda (fn)
;; :        (printf "loading ~a ..." fn)
;; :        (load fn)
;; :        (printf "~%"))
;; :      fns)
;; :    (new-cafe)))

(test factorial)
(test factorial-d)
(exit)
