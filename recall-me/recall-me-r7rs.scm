;;========================================================================
;; recall-me-r7rs.scm
; a true r7rs sample program with libraries
;;========================================================================

(define-library (factorial main)
                (export factorial-d factorial)
                (import (scheme base) (scheme write) (scheme read))

                (begin

                  (define factorial
                    (lambda(N)
                      (letrec ((ifactorial (lambda(acc k)
                                             (if (> k 1)
                                                 (let ((n-acc (* k acc))
                                                       (n-k (- k 1)))
                                                   (ifactorial n-acc n-k))
                                                 acc
                                                 ))))
                        (ifactorial 1 N))))

                  (define factorial-d
                    (lambda(N)
                      (define ifactorial (lambda(acc k)
                                           (if (> k 1)
                                               (let ((n-acc (* k acc))
                                                     (n-k (- k 1)))
                                                 (ifactorial n-acc n-k))
                                               acc
                                               )))
                      (ifactorial 1 N)))))

(define-library (factorial test)
                (export test)
                (import (scheme base) (scheme write) (scheme read)
                        (factorial main))
                (begin
                  (define a-test (lambda (k l-factorial)
                                   (display k)
                                   (display "! = ")
                                   (display (l-factorial k))
                                   (newline)))

                  (define test (lambda (l-factorial)
                                 (a-test 1 l-factorial)
                                 (a-test 2 l-factorial)
                                 (a-test 4 l-factorial)
                                 (a-test 20 l-factorial)))))

(import (factorial main) (factorial test))
(test factorial)
(test factorial-d)
