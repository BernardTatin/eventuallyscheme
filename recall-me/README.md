# README: recall-me

Some code to test generic functions (à la Lisp), some _Scheme_ environment.

## _PLAIT_

`recall-me-plait.rkt`: It is a language from the _Racket_ planet, a sort of typed _Scheme. Nice but not
for production, I think. I won't use it.

## _gosh_

`recall-me-gosh.scm`: _Gosh_ has its own CLOS version with generic functions which work
really well. The [library index](http://practical-scheme.net/gauche/man/gauche-refe/index.html#Top) is big,
I will find all what I need.
