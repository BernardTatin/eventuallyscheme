#lang plait

#|
  recall-me-plait.rkt
  plait is a typed Scheme from the Racket System
|#

;; we can't put this define in another define
;; to make the function private
(define ifactorial : (Number Number -> Number)
      (lambda(acc k)
        (if (> k 1)
            (let ((n-acc (* k acc))
                  (n-k (- k 1)))
              (ifactorial n-acc n-k))
            acc)))

(define factorial : (Number -> Number)
 (lambda (n)
    (ifactorial 1 n)))

;; but we can use letrec
(define factorial-d : (Number -> Number)
  (lambda (n)
    (letrec ((ifactorial-d
              (lambda(acc k)
                (if (> k 1)
                    (let ((n-acc (* k acc))
                          (n-k (- k 1)))
                      (ifactorial-d n-acc n-k))
                    acc))))
      (ifactorial-d 1 n))))

;; oups, newline not defined?
;; had to search an import?
;; well, rewrite it
(define newline : ( -> Void)
  (lambda()
    (display "\n")))

(define a-test : (Number (Number -> Number) -> Void)
  (lambda (k l-factorial)
    (begin
      (display k)
      (display "! = ")
      (display (l-factorial k))
      (newline))))

(define test : ((Number -> Number) -> Void)
  (lambda (l-factorial)
    (begin
      (a-test 1 l-factorial)
      (a-test 2 l-factorial)
      (a-test 4 l-factorial)
      (a-test 20 l-factorial))))

(begin
  (test factorial)
  (test factorial-d))
