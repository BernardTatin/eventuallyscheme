;;========================================================================
;; recall-me-r6rs-main.scm
;; a true r6rs sample program with libraries
;; in different SLD files 
;;========================================================================

(import (rnrs) (factorial-r6 main) (factorial-r6 test))

(test factorial)
(test factorial-d)
(exit)
